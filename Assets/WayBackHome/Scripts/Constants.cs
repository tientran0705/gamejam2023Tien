using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public static short MAX_SPEED = 1500;
    public const string KEY_SPAWN_RUNNER = "KEY_SPAWN_RUNNER";
    public const short healthItem = 10;
    public const short energyItem = 10;
    public const short fuelItem = 10;
    public const short dollarItem = 10;
    public const float timeSkill1 = 5f;
    public const float timeSkill1_2 = 4f;
    public const short energySkill = 10;
    public const float timeReturnSkill1 = 4f;
}

public enum TypeItem 
{
    HEALTH,
    ENERGY,
    FUEL,
    DOLLAR
}



