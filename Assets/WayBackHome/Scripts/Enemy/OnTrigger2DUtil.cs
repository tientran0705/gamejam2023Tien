using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTrigger2DUtil : MonoBehaviour
{
    public string targetTag = "Player";
    public List<EnemyAI> enemyAIs = new List<EnemyAI>();
    public UnityEvent OnTriggerEnterEvent, OnTriggerExitEvent, OnTriggerStayEvent;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(targetTag))
        {
            OnTriggerStayEvent?.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(targetTag))
        {
            OnTriggerEnterEvent?.Invoke();
            foreach(EnemyAI enemyAI in enemyAIs)
            {
                enemyAI.SetTarget(collision.gameObject.transform);
                enemyAI.StartPathEnemy();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(targetTag))
        {
            OnTriggerExitEvent?.Invoke();
        }
    }
}
