using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public RuntimeAnimatorController[] animators;
    public Sprite[] sprites;
    private Rigidbody2D _rb;
    public Animator _anim;
    public SpriteRenderer _renderer;
    private PlayerRigidBodyMovement _movement;

    void Start()
    {
        _rb = GetComponentInParent<Rigidbody2D>();
        _movement = GetComponentInParent<PlayerRigidBodyMovement>();
        
        _renderer = GetComponent<SpriteRenderer>();
    }

    public void SetAnimator(int index)
    {
        _anim.runtimeAnimatorController = animators[index];
        _renderer.sprite = sprites[index];
    }

    void LateUpdate()
    {
        if (_rb.velocity.x < -.1f)
        {
            _renderer.flipX = true;
        }else if (_rb.velocity.x > .1f)
        {
            _renderer.flipX = false;
        }

        _anim.SetBool("Grounded", _movement.GetGrounded());
        _anim.SetFloat("Y_Speed", _rb.velocity.y);
        _anim.SetFloat("X_Speed_Abs", Mathf.Abs(_rb.velocity.x));
    }
}
