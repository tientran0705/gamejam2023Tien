using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : Singleton<PlayerManager>
{
    private PlayerBehaviour playerBehaviourLocal;
    private int IDPlayerLocal;

    private void Start()
    {
    }

    public int GetCurrentHp()
    {
        if(playerBehaviourLocal != null)
        return playerBehaviourLocal.currentHPValue;
        return -1;
    }

    public void SetPlayerBehaviourLocal(PlayerBehaviour playerBehaviour)
    {
        this.playerBehaviourLocal = playerBehaviour;
    }

    public void SetIDPlayerLocal(int id)
    {
        IDPlayerLocal = id;
    }

    public int GetIDPlayerLocal()
    {
        return IDPlayerLocal;
    }
}
