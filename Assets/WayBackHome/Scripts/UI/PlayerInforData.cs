using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerInforData
{
    public int level;
    public int health;
    public int maxSpeed;
    public int energy;
    public int fuel;
    public int dollar;

    public int currentEngineSld;
    public int currentTankerSld;
    public int currentWheelSld;

    public PlayerInforData(Player player)
    {
        level = player.level;
        health = player.health;
        maxSpeed = player.maxSpeed;
        energy = player.energy;
        fuel = player.fuel;
        dollar = player.dollar;

        currentEngineSld = player.currentEngineSld;
        currentTankerSld = player.currentTankerSld;
        currentWheelSld = player.currentWheelSld;
    }
}
