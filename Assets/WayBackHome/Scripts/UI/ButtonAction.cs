using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[Serializable]
public class ButtonAction : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public string id;
    public ButtonSate stateAction;


    public void OnPointerDown(PointerEventData eventData)
    {
        stateAction = ButtonSate.onPressed;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        stateAction = ButtonSate.none;
    }
}

public enum ButtonSate
{
    none,
    onClick,
    onPressed,
    onSelected
}
