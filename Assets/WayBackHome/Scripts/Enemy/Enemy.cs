using Fusion;
using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;

public class Enemy : NetworkBehaviour
{
    [SerializeField] PlayerBehaviour playerBehaviour;
    [SerializeField] SliderBar HpSlider;
    [SerializeField] short damage;
    [SerializeField] short maxHp;
    [SerializeField] short maxSpeed = 2500;
    [SerializeField] EnemyAI enemyAI;
    [SerializeField] Animator animator;
    [Networked]
    public NetworkObject Instance { get; set; }
    [Networked] TickTimer tickTimer { get; set; }//for attacking

    private Collider2D _hitCollider2D;
    [SerializeField]private Collider2D _collider;
    private Collider2D[] colliders;
    [Networked(OnChanged = nameof(UpdateCurrentHP))]
    short currentHp { get; set; }
    [Networked] short speed { get; set; }

    public static void UpdateCurrentHP(Changed<Enemy> changed)
    {
        //if(Object.HasStateAuthority)
        //changed.Behaviour.UpdateRateHP();
        changed.Behaviour.UpdateRateHP();
    }

    void UpdateRateHP()
    {
        HpSlider.SetValueBar(currentHp);
    }



    private void Awake()
    {
       // _collider = GetComponentInChildren<Collider2D>();
        
    }

    public override void Spawned()
    {
        currentHp = maxHp;
        HpSlider.SetMaxValue(maxHp);
        if (CheckEnemyAIEnable())
        {
            enemyAI.speed = maxSpeed;
        }
    }

    public override void FixedUpdateNetwork()
    {
        DetectCollisions();
    }

    ContactFilter2D filter2D;
    protected void DetectCollisions()
    {
        _hitCollider2D = Runner.GetPhysicsScene2D().OverlapBox(transform.position, _collider.bounds.size * 1.1f, 0, LayerMask.GetMask("Player"));
        
        if (_hitCollider2D != default)
        {
            filter2D.layerMask = LayerMask.GetMask("Player");

            if (playerBehaviour != null)
            {
                OnAttackPlayer();
                SetAnimateAttack("Attack 2");
                playerBehaviour.TakeDamage(damage);
                
            }
        }
    }

    private bool CheckEnemyAIEnable()
    {
        if(enemyAI != null)
        {
            return true;
        }
        return false;
    }

    public void SetPlayerBehaviour()
    {
        if(_hitCollider2D != null)
        playerBehaviour = _hitCollider2D.gameObject.GetComponentInParent<PlayerBehaviour>();
    }

    public void SetAnimateAttack(string animateKey)
    {
        if (tickTimer.ExpiredOrNotRunning(Runner))
        {
            animator.SetTrigger(animateKey);
            tickTimer = TickTimer.CreateFromSeconds(Runner, 0.5f);
        }
    }

    public void SetAnimateInjured(string animateKey)
    {
        animator.SetTrigger(animateKey);
    }

    public void TakeDamage(short amount)
    {
        currentHp -= amount;
        SetAnimateInjured("Hit");
        if(currentHp < 0)
        RemoveEnemy();
    }

    private void RemoveEnemy()
    {
        Runner.Despawn(Instance);
    }

    //override it and consists of effects of attacking
    protected virtual void OnAttackPlayer()
    {
        
    }
}
