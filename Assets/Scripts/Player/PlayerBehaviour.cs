using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
using Unity.Mathematics;
using static UnityEngine.GraphicsBuffer;
using UnityEngine.PlayerLoop;

public class PlayerBehaviour : NetworkBehaviour
{
    public PlayerAnimation PlayerAnimation;
    public Transform CameraTransform;

    [Networked(OnChanged = nameof(OnNickChanged))]
    public NetworkString<_16> Nickname { get; set; }
    [Networked]
    public Color PlayerColor { get; set; }

    public int PlayerID { get; private set; }

    private NetworkRigidbody2D _rb;
    private InputController _inputController;
    private Collider2D _collider;
    private Collider2D _hitCollider;
    [SerializeField] private PlayerRigidBodyMovement playerRigidBodyMovement;

    [Networked]
    private TickTimer RespawnTimer { get; set; }
    [Networked(OnChanged = nameof(OnSpawningChange))]
    private NetworkBool Respawning { get; set; }
    [Networked]
    private NetworkBool Finished { get; set; }
    [Networked]
    public NetworkBool InputsAllowed { get; set; }


    [SerializeField] private ParticleManager _particleManager;

    [Space()]
    [Header("Sound")]
    [SerializeField] private SoundChannelSO _sfxChannel;
    [SerializeField] private SoundSO _deathSound;
    [SerializeField] private AudioSource _playerSource;

    private LevelBehaviour _levelBehaviour;
    private void Awake()
    {
        _inputController = GetBehaviour<InputController>();
        _rb = GetBehaviour<NetworkRigidbody2D>();
        _collider = GetComponentInChildren<Collider2D>();
        //StartCoroutine(GetPlayerBehaviour());
    }

    public Player player;
    public override void Spawned()
    {
        PlayerID = Object.InputAuthority;
        Debug.Log("ID Player " + PlayerID);
        if (Object.HasInputAuthority)
        {
            PlayerManager.Instance.SetIDPlayerLocal(PlayerID);// Set id of player to recognize at local
            //Set Interpolation data source to predicted if is input authority.
            _rb.InterpolationDataSource = InterpolationDataSources.Predicted;
            CameraManager camera = FindObjectOfType<CameraManager>();
            camera.CameraTarget = CameraTransform;

            if (Nickname == string.Empty)
            {
                RPC_SetNickname(PlayerPrefs.GetString("Nick"));
            }
            RPC_SetMotorPlayer((byte)RunnerSaveController.Instance.GetIndexSpawnRunner());
            player.LoadPlayer();
            RPC_SetInforPlayer((short)player.health, (short)player.energy, (short)player.fuel, (short)player.maxSpeed);
            GetComponentInChildren<SpriteRenderer>().sortingOrder += 1;
            _levelBehaviour = FindObjectOfType<LevelBehaviour>();
            //PlayerManager.Instance.SetPlayerBehaviourLocal(this);
            SetPerformAttackPlayer(true);

        }

        if (Object.HasInputAuthority)
        {
            playerInfor.gameObject.SetActive(true);

            RPC_InitInfoPlayer();
        }



        GetComponentInChildren<NicknameText>().SetupNick(Nickname.ToString());
        //GetComponentInChildren<SpriteRenderer>().color = PlayerColor;
        _particleManager.ClearParticles();
    }

    [Rpc(sources: RpcSources.InputAuthority, targets: RpcTargets.StateAuthority)]
    public void RPC_SetNickname(string nick)
    {
        Nickname = nick;
    }

    public static void OnNickChanged(Changed<PlayerBehaviour> changed)
    {
        changed.Behaviour.OnNickChanged();
    }

    public void SetInputsAllowed(bool value)
    {
        InputsAllowed = value;
    }

    private void SetRespawning()
    {
        if (Runner.IsServer)
        {
            RPC_DeathEffects();
            _rb.Rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

    /// <summary>
    /// Confirm death effect on GFX in case a client-side predicted death was wrong.
    /// </summary>
    /// <param name="changed"></param>
    public static void OnSpawningChange(Changed<PlayerBehaviour> changed)
    {
        if (changed.Behaviour.Respawning)
        {
            changed.Behaviour.SetGFXActive(false);
        }
        else
        {
            changed.Behaviour.SetGFXActive(true);
        }
    }

    [Rpc(sources: RpcSources.StateAuthority, targets: RpcTargets.All)]
    private void RPC_DeathEffects()
    {
        _particleManager.Get(ParticleManager.ParticleID.Death).transform.position = transform.position;
        PlayDeathSound();
    }

    private void SetGFXActive(bool value)
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(value);
    }

    private void OnNickChanged()
    {
        GetComponentInChildren<NicknameText>().SetupNick(Nickname.ToString());
    }

    public override void FixedUpdateNetwork()
    {

        DetectCollisions();

        if (GetInput<InputData>(out var input) && InputsAllowed)
        {
            if (input.GetButtonPressed(_inputController.PrevButtons).IsSet(InputButton.RESPAWN) && !Respawning)
            {
                RequestRespawn();
            }

            UpdateSkill(input);
        }

        if (Object.HasInputAuthority || Object.HasStateAuthority)
        {
            EndSkill1();
        }
        playerInforBase.Health.SetValueBar(currentHPValue);
        playerInforBase.Energy.SetValueBar(currentEnergyValue);

        if (Respawning)
        {
            if (RespawnTimer.Expired(Runner))
            {
                _rb.Rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
                StartCoroutine(Respawn());
            }
        }
    }

    private void PlayDeathSound()
    {
        _sfxChannel.CallSoundEvent(_deathSound, Object.HasInputAuthority ? null : _playerSource);
    }

    private IEnumerator Respawn()
    {
        _rb.TeleportToPosition(PlayerSpawner.PlayerSpawnPos);
        yield return new WaitForSeconds(.1f);
        Respawning = false;
        SetInputsAllowed(true);
    }

    private void FinishRace()
    {
        if (Finished) { return; }

        if (Object.HasInputAuthority)
        {
            GameManager.Instance.SetPlayerSpectating(this);
        }

        if (Runner.IsServer)
        {
            _levelBehaviour.PlayerOnFinishLine(Object.InputAuthority, this);
            Finished = true;
        }
    }

    public void RequestRespawn()
    {
        Respawning = true;
        SetInputsAllowed(false);
        RespawnTimer = TickTimer.CreateFromSeconds(Runner, 1f);
        SetRespawning();
    }

    private void DetectCollisions()
    {
        _hitCollider = Runner.GetPhysicsScene2D().OverlapBox(transform.position, _collider.bounds.size * 1.1f, 0, LayerMask.GetMask("Interact"));
        if (_hitCollider != default)
        {
            if (_hitCollider.tag.Equals("Kill") && !Respawning)
            {
                RequestRespawn();
            }
            else if (_hitCollider.tag.Equals("Finish") && !Finished)
            {
                FinishRace();
            }
            else if (_hitCollider.tag.Equals("Enemy") && !Respawning)
            {
                // TakeDamage(10);
                //Debug.Log("Player die");
            }
        }
    }

    [Rpc(sources: RpcSources.InputAuthority, targets: RpcTargets.All)]
    void RPC_InitInfoPlayer()
    {
        playerInfor.Health.SetMaxValue(maxHP);
        playerInfor.Energy.SetMaxValue(maxEnergy);
        playerInfor.Fuel.SetMaxValue(maxFuel);
        playerInforBase.Health.SetMaxValue(maxHP);
        playerInforBase.Energy.SetMaxValue(maxEnergy);

        currentHPValue = maxHP;
        currentEnergyValue = maxEnergy;
        currentFuelValue = maxFuel;
        Debug.Log("Client said");

    }

    #region health
    public short maxHP;
    //public float currentHPValue;
    public PlayerInfor playerInfor;
    [SerializeField] private PlayerInforBase playerInforBase;
    [Networked] private TickTimer tickDamage { get; set; }
    [Networked(OnChanged = nameof(UpdateCurrentHP))]
    public short currentHPValue { get; set; }
    public static void UpdateCurrentHP(Changed<PlayerBehaviour> changed)
    {
        //if(Object.HasStateAuthority)
        changed.Behaviour.UpdateRateHP();
    }

    public void UpdateRateHP()
    {
        UpdateHPAtLocal();

        Debug.Log("HP current: " + currentHPValue);
        Debug.Log("HP max: " + maxHP);
    }

    private void UpdateHPAtLocal()
    {
        if (PlayerManager.Instance.GetIDPlayerLocal() == PlayerID)
        {
            playerInfor.Health.SetValueBar(currentHPValue);
            if (currentHPValue <= 0) _levelBehaviour.RPC_ResetLevel();
            //playerInforBase.Health.SetValueBar(currentHPValue);
        }
    }

    private Vector2 direction;
    public void TakeDamage(short amount)
    {

        if (tickDamage.ExpiredOrNotRunning(Runner) && GetPerformAttackPlayer())
        {
            currentHPValue -= amount;

            Debug.Log("Player take damage " + tickDamage.IsRunning);
            if (currentHPValue <= 0)
            {
                Debug.Log("Player die");
            }
            //_rb.Rigidbody.velocity *= Vector2.left*100;
            //if(Object.HasInputAuthority)
            if(_rb.Rigidbody.velocity.normalized == Vector2.zero)
            {
                direction = Vector2.left;
            }
            else
            {
                direction = _rb.Rigidbody.velocity.normalized;
            }
            _rb.Rigidbody.AddForce(-direction * 50, ForceMode2D.Impulse);
            StartCoroutine(ResetKnockBack());
            tickDamage = TickTimer.CreateFromSeconds(Runner, 1f);

        }
    }

    private IEnumerator ResetKnockBack()
    {
        yield return new WaitForSeconds(0.1f);
        _rb.Rigidbody.velocity = Vector3.zero;
    }

    public void AddHealthItem(short amount)
    {
        currentHPValue += amount;
    }
    //public override void Despawned(NetworkRunner runner, bool hasState)
    //{
    //    base.Despawned(runner, hasState);
    //}

    //IEnumerator GetPlayerBehaviour()
    //{
    //    yield return new WaitUntil(()=>FindObjectOfType<LevelBehaviour>());

    //}
    #endregion

    #region energy
    [Networked(OnChanged = nameof(UpdateCurrentEnergy))]
    public short currentEnergyValue { get; set; }

    public short maxEnergy;

    public static void UpdateCurrentEnergy(Changed<PlayerBehaviour> changed)
    {
        changed.Behaviour.UpdateRateEnergy();
    }

    private void UpdateRateEnergy()
    {
        UpdateEnergyAtLocal();
    }

    private void UpdateEnergyAtLocal()
    {
        if (PlayerManager.Instance.GetIDPlayerLocal() == PlayerID)
        {
            playerInfor.Energy.SetValueBar(currentEnergyValue);
            playerInforBase.Energy.SetValueBar(currentEnergyValue);
        }
    }

    public void AddEnergyItem(short amount)
    {
        currentEnergyValue += amount;
    }
    private void TakeEnergy(short amount)
    {
        currentEnergyValue -= amount;
    }
    #endregion

    #region

    [Networked(OnChanged = nameof(UpdateCurrentSpeed))]
    NetworkBool changeStateMove { get; set; }

    public void ChangeStateMove()
    {
        changeStateMove = !changeStateMove;
        //changeStateMove = !changeStateMove;
    }

    private static void UpdateCurrentSpeed(Changed<PlayerBehaviour> changed)
    {
        changed.Behaviour.UpdateSpeedLocal();
    }

    private void UpdateSpeedLocal()
    {
        playerInfor.SpeedometerMotor.SetCurrentSpeed(playerRigidBodyMovement.GetCurrentSpeed());
    }

    #endregion

    #region fuel

    [Networked(OnChanged = nameof(UpdateCurrentFuel))]

    public short currentFuelValue { get; set; }

    public short maxFuel;

    private static void UpdateCurrentFuel(Changed<PlayerBehaviour> changed)
    {
        changed.Behaviour.UpdateFuelLocal();
    }

    private void UpdateFuelLocal()
    {
        if (PlayerManager.Instance.GetIDPlayerLocal() == PlayerID)
            playerInfor.Fuel.SetValueBar(currentFuelValue);
    }

    public void TakeFuel(short amount)
    {
        currentFuelValue -= amount;
    }

    public void AddFuel(short amount)
    {
        currentFuelValue += amount;
    }
    #endregion

    #region load data local
    [Rpc(sources: RpcSources.InputAuthority, targets: RpcTargets.All)]
    public void RPC_SetMotorPlayer(byte index)
    {
        Debug.Log("Index animator " + index);
        PlayerAnimation.SetAnimator(index);
    }

    
    [Rpc(sources: RpcSources.InputAuthority, targets: RpcTargets.All)]
    public void RPC_SetInforPlayer(short r_maxHP, short r_maxEnergy, short r_maxFuel, short r_speed)
    {
        maxHP = (short)r_maxHP;
        maxEnergy = (short) r_maxEnergy;
        maxFuel = (short) r_maxFuel;
        playerRigidBodyMovement.SetSpeed(r_speed);
        
    }
    #endregion

    #region update dollar
    
    public void UpdateDollar(short amount)
    {
        player.AddDollar(amount);
        player.SavePlayer();
    }

    #endregion

    public PlayerBehaviour friendPlayer;
    void SetFriendPlayer()
    {
        friendPlayer = FindObjectOfType<PlayerBehaviour>();
    }

    void UpdateSkill(InputData input)
    {
        if ((byte)RunnerSaveController.Instance.GetIndexSpawnRunner() == 0)
        {
            UpdateSkill1(input);
        }
        else
        {
            UpdateSkill1_2(input);
        }
    }


    #region skill1
    [SerializeField] NetworkObject skill1Collider;
    [Networked] TickTimer tickSkill_1 { get; set; }
    [Networked] TickTimer tickTimerReturnSKill1 { get; set; }
    [Networked] bool isPerformAttackPlayer { get; set; }

    private void UpdateSkill1(InputData input)
    {
        if (input.GetButton(InputButton.SKILL1) && InputsAllowed && CanPerformSkill1())
        {
            //playerRigidBodyMovement.SetSpeed(5000);
            TakeEnergy(100);
            SetPerformAttackPlayer(false);
            if(friendPlayer != null)
            {
                friendPlayer.SetPerformAttackPlayer(false);
            }
            tickSkill_1 = TickTimer.CreateFromSeconds(Runner, Constants.timeSkill1);
        }
    }

    public void SetPerformAttackPlayer(bool performValue)
    {
        isPerformAttackPlayer = performValue;
    }

    public bool GetPerformAttackPlayer()
    {
        return isPerformAttackPlayer;
    }

    private bool CanPerformSkill1()
    {   if (tickTimerReturnSKill1.IsRunning) 
        {
            return false;
        }
        return tickSkill_1.ExpiredOrNotRunning(Runner) && currentEnergyValue>Constants.energySkill;
    }

    private void EndSkill1()
    {
        if (tickSkill_1.Expired(Runner))
        {
            tickTimerReturnSKill1 = TickTimer.CreateFromSeconds(Runner, Constants.timeReturnSkill1);
            SetPerformAttackPlayer(true);
            if (friendPlayer != null)
            {
                friendPlayer.SetPerformAttackPlayer(true);
            }
        }
    }
    #endregion

    #region skill1_2
    [SerializeField] NetworkObject skill1_2Collider;
    [Networked] TickTimer tickSkill_1_2 { get; set; }
    private void UpdateSkill1_2(InputData input)
    {
        if (input.GetButton(InputButton.SKILL1) && InputsAllowed && CanPerformSkill1_2())
        {
            //playerRigidBodyMovement.SetSpeed(5000);
            TakeEnergy(100);
            friendPlayer.AddHealthItem(Constants.healthItem);
            tickSkill_1_2 = TickTimer.CreateFromSeconds(Runner, Constants.timeSkill1_2);
        }
    }

    private bool CanPerformSkill1_2()
    {
        return tickSkill_1_2.ExpiredOrNotRunning(Runner) && currentEnergyValue > Constants.energySkill;
    }

    private void EndSkill2()
    {
        if (tickSkill_1_2.Expired(Runner))
        {
            playerRigidBodyMovement.SetSpeed(Constants.MAX_SPEED);
        }
    }
    #endregion
}
