using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SavePlayer(Player player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerInforData data = new PlayerInforData(player);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static PlayerInforData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.fun";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerInforData data = formatter.Deserialize(stream) as PlayerInforData;
            stream.Close();

            return data;

        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }

    public static void ResetPlayer()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        File.Delete(Application.persistentDataPath + "/player.fun");
    }
}
