using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectItem : MonoBehaviour
{
    [SerializeField] private TypeItem typeItem;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision != null && collision.gameObject.tag == "Player")
        {
            CheckCollisionGame(collision);
            gameObject.SetActive(false);
        }
    }

    void CheckCollisionGame(Collider2D collision)
    {
        switch (typeItem)
        {
            case TypeItem.HEALTH:
                collision.gameObject.GetComponentInParent<PlayerBehaviour>().AddHealthItem(Constants.healthItem);
                break;
            case TypeItem.ENERGY:
                collision.gameObject.GetComponentInParent<PlayerBehaviour>().AddEnergyItem(Constants.energyItem);
                break;
            case TypeItem.FUEL:
                collision.gameObject.GetComponentInParent<PlayerBehaviour>().AddFuel(Constants.fuelItem);
                break;
            case TypeItem.DOLLAR:
                collision.gameObject.GetComponentInParent<PlayerBehaviour>().UpdateDollar(Constants.dollarItem);
                break;
        }
    }
}
