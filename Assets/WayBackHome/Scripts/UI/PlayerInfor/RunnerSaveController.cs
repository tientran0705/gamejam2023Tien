using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RunnerSaveController : Singleton<RunnerSaveController>
{
    public void SetIndexSpawnRunner(int level)
    {
        PlayerPrefs.SetInt(Constants.KEY_SPAWN_RUNNER, level);
    }

    public int GetIndexSpawnRunner()
    {
        return PlayerPrefs.GetInt(Constants.KEY_SPAWN_RUNNER, 0);
    }
}
