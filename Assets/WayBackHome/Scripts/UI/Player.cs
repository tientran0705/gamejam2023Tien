using System.Runtime.Serialization;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int level = 3;
    public int health = 40;
    public int maxSpeed = 1500;
    public int energy = 100;
    public int fuel = 100;
    public int dollar = 100;

    public int currentEngineSld = 0;
    public int currentTankerSld = 0;
    public int currentWheelSld = 0;

    [ContextMenu("Reset Game")]
    public void ResetPlayer()
    {
        SaveSystem.ResetPlayer();
    }

    public void SavePlayer()
    {
        SaveSystem.SavePlayer(this);
    }

    public void LoadPlayer()
    {
        PlayerInforData data = SaveSystem.LoadPlayer();
        if (data == null) return;
        level = data.level;
        health = data.health;
        maxSpeed = data.maxSpeed;
        energy = data.energy;
        fuel = data.fuel;
        dollar = data.dollar; 
        currentEngineSld = data.currentEngineSld;
        currentTankerSld = data.currentTankerSld;
        currentWheelSld = data.currentWheelSld;
    }

    #region UI Methods

    public void ChangeLevel(int amount)
    {
        level += amount;
    }

    public void ChangeHealth(int amount)
    {
        health += amount;
    }

    public void ChangeSpeed(int amount)
    {
        this.maxSpeed += amount;
    }

    public void ChangeEnergy(int amount)
    {
        this.energy += amount;
    }
    
    public void ChangeFuel(int amount)
    {
        fuel += amount;
    }

    public void AddDollar(int amount)
    {
        dollar += amount;
    }

    public void SubDollar(int amount)
    {
        dollar -= amount;
    }
    #endregion
}