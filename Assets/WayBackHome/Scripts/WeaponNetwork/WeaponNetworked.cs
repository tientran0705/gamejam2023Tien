using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponNetworked : NetworkBehaviour
{
    public int rotationOffset = 0;

    public float Damage = 10f;
    public LayerMask whatToHit;// setting for collision with collider

    public Transform BulletTrailPrefab;
    public Transform MuzzleFlashPrefab;

    public Transform firePoint;

    public Transform targetEnemy;
    public Collider2D _colliderRange;
    public Collider2D[] _hitEnemies;
    public RaycastHit2D _raycastHit2D;

    public float RangeWeapon;

    [Networked] TickTimer tickDurationAttack { get; set; }
    public float timeFire = 0.15f;

    [Header("Sound")]
    [SerializeField] private SoundChannelSO _sfxChannel;
    [SerializeField] private SoundSO _shootSound;
    [SerializeField] private AudioSource _weaponSource;

    private void Awake()
    {
      //firePoint = transform.Fi
      if(firePoint == null)
        {
            Debug.LogError("No firePoint");
        }
    }

    public override void FixedUpdateNetwork()
    {
        SetListEnemyOverlap();
        SetNearestEnemy();
        firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
        if (nearestEnemy != null && _hitEnemies.Length > 0)
        {
            if (tickDurationAttack.ExpiredOrNotRunning(Runner))
            {
                DetectCollision();
                Debug.DrawLine(firePointPosition, ((Vector2)targetEnemy.position - firePointPosition) * 100, Color.cyan);
                tickDurationAttack = TickTimer.CreateFromSeconds(Runner, timeFire);
            }
        }
        
        if (firePointPosition != null && nearestEnemy != null)
        {
            RotationWeapon();
            
        }
    }

    [Rpc(sources: RpcSources.StateAuthority, targets: RpcTargets.All)]
    private void RPC_PlayShootSound()
    {
        PlayShootSound();
    }

    private void PlayShootSound()
    {
        _sfxChannel.CallSoundEvent(_shootSound, Object.HasInputAuthority ? null : _weaponSource);
    }

    [Networked] Vector2 firePointPosition { get; set; }
    public override void Spawned()
    {
        
    }

    
    void DetectCollision()
    {
        _raycastHit2D = Runner.GetPhysicsScene2D().Raycast(firePointPosition, (Vector2) nearestEnemy.transform.position - firePointPosition, RangeWeapon, whatToHit);
        Effect();
        RPC_PlayShootSound();
        if (_raycastHit2D.collider != null)
        {
            Debug.DrawLine(firePointPosition, _raycastHit2D.point, Color.red);
            Debug.Log("We hit " + _raycastHit2D.collider.name + " and did " + Damage + " damage.");
            _raycastHit2D.collider.gameObject.GetComponentInParent<Enemy>().TakeDamage(10);
        }
    }

    void RotationWeapon()
    {
        Vector3 difference = targetEnemy.transform.position - transform.position;
        difference.Normalize();

        float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotZ + rotationOffset);
        
    }
    void Effect()
    {
        Instantiate(BulletTrailPrefab, firePoint.position, firePoint.rotation);
        Transform clone = Instantiate(MuzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
        clone.parent = firePoint;
        float size = Random.Range(0.6f, 0.9f);
        clone.localScale = new Vector3(size, size, size);
        //yield return 0;
        Destroy(clone.gameObject,0.02f);
    }

    void SetListEnemyOverlap()
    {
        _hitEnemies = Physics2D.OverlapBoxAll(transform.position, _colliderRange.bounds.size, 0f, whatToHit);
    }

    float nearestDistEnemy = 100f;
    GameObject nearestEnemy = null;
    public void SetNearestEnemy()
    {
        
        float distance;
        foreach(Collider2D collider2D in _hitEnemies)
        {
            distance = Vector2.Distance(transform.position, collider2D.gameObject.transform.position);
            if (nearestDistEnemy > distance)
            {
                Debug.Log("Collider 2d: " + collider2D.gameObject);
                nearestDistEnemy = distance;
                nearestEnemy = collider2D.gameObject;
            }
        }
        if(nearestEnemy != null)
        targetEnemy = nearestEnemy.transform;
    }
}